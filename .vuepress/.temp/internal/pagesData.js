export const pagesData = {
  // path: /
  "v-8daa1a0e": () => import(/* webpackChunkName: "v-8daa1a0e" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/index.html.js").then(({ data }) => data),
  // path: /intro.html
  "v-184f4da6": () => import(/* webpackChunkName: "v-184f4da6" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/intro.html.js").then(({ data }) => data),
  // path: /recommend.html
  "v-0f3bde54": () => import(/* webpackChunkName: "v-0f3bde54" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/recommend.html.js").then(({ data }) => data),
  // path: /FinOps/finops-define.html
  "v-83ee2532": () => import(/* webpackChunkName: "v-83ee2532" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/FinOps/finops-define.html.js").then(({ data }) => data),
  // path: /FinOps/intro.html
  "v-2ddc7748": () => import(/* webpackChunkName: "v-2ddc7748" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/FinOps/intro.html.js").then(({ data }) => data),
  // path: /GitOps/ArgoCD.html
  "v-20324ac9": () => import(/* webpackChunkName: "v-20324ac9" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/GitOps/ArgoCD.html.js").then(({ data }) => data),
  // path: /GitOps/CD.html
  "v-07d9423c": () => import(/* webpackChunkName: "v-07d9423c" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/GitOps/CD.html.js").then(({ data }) => data),
  // path: /GitOps/CI.html
  "v-049b99fd": () => import(/* webpackChunkName: "v-049b99fd" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/GitOps/CI.html.js").then(({ data }) => data),
  // path: /GitOps/CICD.html
  "v-61b4ed3c": () => import(/* webpackChunkName: "v-61b4ed3c" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/GitOps/CICD.html.js").then(({ data }) => data),
  // path: /GitOps/Crossplane.html
  "v-736bb032": () => import(/* webpackChunkName: "v-736bb032" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/GitOps/Crossplane.html.js").then(({ data }) => data),
  // path: /GitOps/Flagger.html
  "v-1181ca07": () => import(/* webpackChunkName: "v-1181ca07" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/GitOps/Flagger.html.js").then(({ data }) => data),
  // path: /GitOps/GitOps.html
  "v-2ef9b803": () => import(/* webpackChunkName: "v-2ef9b803" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/GitOps/GitOps.html.js").then(({ data }) => data),
  // path: /GitOps/Helm.html
  "v-62822f85": () => import(/* webpackChunkName: "v-62822f85" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/GitOps/Helm.html.js").then(({ data }) => data),
  // path: /GitOps/IaC.html
  "v-14535924": () => import(/* webpackChunkName: "v-14535924" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/GitOps/IaC.html.js").then(({ data }) => data),
  // path: /GitOps/Kustomize.html
  "v-31b592f8": () => import(/* webpackChunkName: "v-31b592f8" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/GitOps/Kustomize.html.js").then(({ data }) => data),
  // path: /GitOps/SonarQube.html
  "v-8c2ece36": () => import(/* webpackChunkName: "v-8c2ece36" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/GitOps/SonarQube.html.js").then(({ data }) => data),
  // path: /GitOps/intro.html
  "v-54e01fe3": () => import(/* webpackChunkName: "v-54e01fe3" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/GitOps/intro.html.js").then(({ data }) => data),
  // path: /MessageQueue/intro.html
  "v-55a30499": () => import(/* webpackChunkName: "v-55a30499" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/MessageQueue/intro.html.js").then(({ data }) => data),
  // path: /MessageQueue/mq-benchmark.html
  "v-2575d919": () => import(/* webpackChunkName: "v-2575d919" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/MessageQueue/mq-benchmark.html.js").then(({ data }) => data),
  // path: /MessageQueue/mq-diff.html
  "v-d3c7a292": () => import(/* webpackChunkName: "v-d3c7a292" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/MessageQueue/mq-diff.html.js").then(({ data }) => data),
  // path: /MicroService/Envoy.html
  "v-8bab4bf2": () => import(/* webpackChunkName: "v-8bab4bf2" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/MicroService/Envoy.html.js").then(({ data }) => data),
  // path: /MicroService/Istio.html
  "v-526e160e": () => import(/* webpackChunkName: "v-526e160e" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/MicroService/Istio.html.js").then(({ data }) => data),
  // path: /MicroService/ServiceMesh-Kubernetes.html
  "v-978e303a": () => import(/* webpackChunkName: "v-978e303a" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/MicroService/ServiceMesh-Kubernetes.html.js").then(({ data }) => data),
  // path: /MicroService/ServiceMesh-implement.html
  "v-b37f0634": () => import(/* webpackChunkName: "v-b37f0634" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/MicroService/ServiceMesh-implement.html.js").then(({ data }) => data),
  // path: /MicroService/ServiceMesh.html
  "v-4d94ce7c": () => import(/* webpackChunkName: "v-4d94ce7c" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/MicroService/ServiceMesh.html.js").then(({ data }) => data),
  // path: /MicroService/micro-service-arc.html
  "v-4c3d708d": () => import(/* webpackChunkName: "v-4c3d708d" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/MicroService/micro-service-arc.html.js").then(({ data }) => data),
  // path: /MicroService/micro-service-principle.html
  "v-a63891ba": () => import(/* webpackChunkName: "v-a63891ba" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/MicroService/micro-service-principle.html.js").then(({ data }) => data),
  // path: /MicroService/micro-service.html
  "v-ac2d0e1c": () => import(/* webpackChunkName: "v-ac2d0e1c" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/MicroService/micro-service.html.js").then(({ data }) => data),
  // path: /MicroService/tracing.html
  "v-4edb425c": () => import(/* webpackChunkName: "v-4edb425c" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/MicroService/tracing.html.js").then(({ data }) => data),
  // path: /Observability/intro.html
  "v-44b82798": () => import(/* webpackChunkName: "v-44b82798" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/Observability/intro.html.js").then(({ data }) => data),
  // path: /Observability/monitor-upgrade.html
  "v-1169f952": () => import(/* webpackChunkName: "v-1169f952" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/Observability/monitor-upgrade.html.js").then(({ data }) => data),
  // path: /api-gateway/APISIX.html
  "v-16a5c6ba": () => import(/* webpackChunkName: "v-16a5c6ba" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/api-gateway/APISIX.html.js").then(({ data }) => data),
  // path: /api-gateway/LVS.html
  "v-00b1c4b2": () => import(/* webpackChunkName: "v-00b1c4b2" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/api-gateway/LVS.html.js").then(({ data }) => data),
  // path: /api-gateway/OpenResty.html
  "v-4d8aa656": () => import(/* webpackChunkName: "v-4d8aa656" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/api-gateway/OpenResty.html.js").then(({ data }) => data),
  // path: /api-gateway/api-gateway.html
  "v-a1822bc2": () => import(/* webpackChunkName: "v-a1822bc2" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/api-gateway/api-gateway.html.js").then(({ data }) => data),
  // path: /api-gateway/balance4.html
  "v-3a92c3ac": () => import(/* webpackChunkName: "v-3a92c3ac" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/api-gateway/balance4.html.js").then(({ data }) => data),
  // path: /api-gateway/balance7.html
  "v-3055aff2": () => import(/* webpackChunkName: "v-3055aff2" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/api-gateway/balance7.html.js").then(({ data }) => data),
  // path: /api-gateway/intro.html
  "v-83531938": () => import(/* webpackChunkName: "v-83531938" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/api-gateway/intro.html.js").then(({ data }) => data),
  // path: /api-gateway/limit.html
  "v-0b248775": () => import(/* webpackChunkName: "v-0b248775" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/api-gateway/limit.html.js").then(({ data }) => data),
  // path: /api-gateway/lvs-balance.html
  "v-55857250": () => import(/* webpackChunkName: "v-55857250" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/api-gateway/lvs-balance.html.js").then(({ data }) => data),
  // path: /api-gateway/lvs-mod.html
  "v-2bedc81c": () => import(/* webpackChunkName: "v-2bedc81c" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/api-gateway/lvs-mod.html.js").then(({ data }) => data),
  // path: /api-gateway/nginx-conf.html
  "v-6d95f2ab": () => import(/* webpackChunkName: "v-6d95f2ab" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/api-gateway/nginx-conf.html.js").then(({ data }) => data),
  // path: /api-gateway/openresty-fire.html
  "v-2489d09a": () => import(/* webpackChunkName: "v-2489d09a" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/api-gateway/openresty-fire.html.js").then(({ data }) => data),
  // path: /api-gateway/openresty-module.html
  "v-1e6da064": () => import(/* webpackChunkName: "v-1e6da064" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/api-gateway/openresty-module.html.js").then(({ data }) => data),
  // path: /architecture/arc-guide.html
  "v-0d5f0f5b": () => import(/* webpackChunkName: "v-0d5f0f5b" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/architecture/arc-guide.html.js").then(({ data }) => data),
  // path: /architecture/arc.html
  "v-454e01aa": () => import(/* webpackChunkName: "v-454e01aa" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/architecture/arc.html.js").then(({ data }) => data),
  // path: /architecture/define-cloud-native.html
  "v-79c3e1db": () => import(/* webpackChunkName: "v-79c3e1db" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/architecture/define-cloud-native.html.js").then(({ data }) => data),
  // path: /architecture/intro.html
  "v-763f2910": () => import(/* webpackChunkName: "v-763f2910" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/architecture/intro.html.js").then(({ data }) => data),
  // path: /container/CRI-O.html
  "v-3cd79024": () => import(/* webpackChunkName: "v-3cd79024" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/container/CRI-O.html.js").then(({ data }) => data),
  // path: /container/CRI-in-Kubernetes.html
  "v-17d94876": () => import(/* webpackChunkName: "v-17d94876" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/container/CRI-in-Kubernetes.html.js").then(({ data }) => data),
  // path: /container/Docker-Kubernetes.html
  "v-6e0047ed": () => import(/* webpackChunkName: "v-6e0047ed" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/container/Docker-Kubernetes.html.js").then(({ data }) => data),
  // path: /container/Nydus-image.html
  "v-68c2213b": () => import(/* webpackChunkName: "v-68c2213b" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/container/Nydus-image.html.js").then(({ data }) => data),
  // path: /container/OCI-in-Docker.html
  "v-26c1dce0": () => import(/* webpackChunkName: "v-26c1dce0" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/container/OCI-in-Docker.html.js").then(({ data }) => data),
  // path: /container/OCI.html
  "v-1c9b7d8b": () => import(/* webpackChunkName: "v-1c9b7d8b" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/container/OCI.html.js").then(({ data }) => data),
  // path: /container/RuntimeClass.html
  "v-f1ea68dc": () => import(/* webpackChunkName: "v-f1ea68dc" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/container/RuntimeClass.html.js").then(({ data }) => data),
  // path: /container/container.html
  "v-453ef83f": () => import(/* webpackChunkName: "v-453ef83f" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/container/container.html.js").then(({ data }) => data),
  // path: /container/containerd.html
  "v-88e1d762": () => import(/* webpackChunkName: "v-88e1d762" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/container/containerd.html.js").then(({ data }) => data),
  // path: /container/declarative-api.html
  "v-6efbddae": () => import(/* webpackChunkName: "v-6efbddae" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/container/declarative-api.html.js").then(({ data }) => data),
  // path: /container/dragonfly.html
  "v-7f91daa0": () => import(/* webpackChunkName: "v-7f91daa0" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/container/dragonfly.html.js").then(({ data }) => data),
  // path: /container/harbor.html
  "v-fcb77c34": () => import(/* webpackChunkName: "v-fcb77c34" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/container/harbor.html.js").then(({ data }) => data),
  // path: /container/high-level-runtime.html
  "v-7339364e": () => import(/* webpackChunkName: "v-7339364e" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/container/high-level-runtime.html.js").then(({ data }) => data),
  // path: /container/image-build.html
  "v-1a55e064": () => import(/* webpackChunkName: "v-1a55e064" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/container/image-build.html.js").then(({ data }) => data),
  // path: /container/image.html
  "v-01c64f25": () => import(/* webpackChunkName: "v-01c64f25" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/container/image.html.js").then(({ data }) => data),
  // path: /container/intro.html
  "v-ba93d698": () => import(/* webpackChunkName: "v-ba93d698" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/container/intro.html.js").then(({ data }) => data),
  // path: /container/kata-container.html
  "v-6cefd24a": () => import(/* webpackChunkName: "v-6cefd24a" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/container/kata-container.html.js").then(({ data }) => data),
  // path: /container/low-level-runtime.html
  "v-0fef1f0a": () => import(/* webpackChunkName: "v-0fef1f0a" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/container/low-level-runtime.html.js").then(({ data }) => data),
  // path: /container/registry.html
  "v-ae5f62d6": () => import(/* webpackChunkName: "v-ae5f62d6" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/container/registry.html.js").then(({ data }) => data),
  // path: /container/rootfs.html
  "v-653c0b23": () => import(/* webpackChunkName: "v-653c0b23" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/container/rootfs.html.js").then(({ data }) => data),
  // path: /container/runtime.html
  "v-203a1908": () => import(/* webpackChunkName: "v-203a1908" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/container/runtime.html.js").then(({ data }) => data),
  // path: /distributed-system/2PC.html
  "v-39141871": () => import(/* webpackChunkName: "v-39141871" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/distributed-system/2PC.html.js").then(({ data }) => data),
  // path: /distributed-system/ACID.html
  "v-9a88f882": () => import(/* webpackChunkName: "v-9a88f882" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/distributed-system/ACID.html.js").then(({ data }) => data),
  // path: /distributed-system/BASE.html
  "v-ec1e7baa": () => import(/* webpackChunkName: "v-ec1e7baa" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/distributed-system/BASE.html.js").then(({ data }) => data),
  // path: /distributed-system/Saga.html
  "v-9f33e0d8": () => import(/* webpackChunkName: "v-9f33e0d8" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/distributed-system/Saga.html.js").then(({ data }) => data),
  // path: /distributed-system/Seata.html
  "v-7f0d725a": () => import(/* webpackChunkName: "v-7f0d725a" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/distributed-system/Seata.html.js").then(({ data }) => data),
  // path: /distributed-system/TCC.html
  "v-554b0bc2": () => import(/* webpackChunkName: "v-554b0bc2" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/distributed-system/TCC.html.js").then(({ data }) => data),
  // path: /distributed-system/XA.html
  "v-3d3d09d3": () => import(/* webpackChunkName: "v-3d3d09d3" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/distributed-system/XA.html.js").then(({ data }) => data),
  // path: /distributed-system/cap.html
  "v-297c9b38": () => import(/* webpackChunkName: "v-297c9b38" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/distributed-system/cap.html.js").then(({ data }) => data),
  // path: /distributed-system/compensate.html
  "v-09cead76": () => import(/* webpackChunkName: "v-09cead76" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/distributed-system/compensate.html.js").then(({ data }) => data),
  // path: /distributed-system/consensus.html
  "v-4eed5b1f": () => import(/* webpackChunkName: "v-4eed5b1f" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/distributed-system/consensus.html.js").then(({ data }) => data),
  // path: /distributed-system/consistency.html
  "v-05ade2de": () => import(/* webpackChunkName: "v-05ade2de" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/distributed-system/consistency.html.js").then(({ data }) => data),
  // path: /distributed-system/distributed-transaction.html
  "v-6ae46042": () => import(/* webpackChunkName: "v-6ae46042" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/distributed-system/distributed-transaction.html.js").then(({ data }) => data),
  // path: /distributed-system/idempotent.html
  "v-40a1e883": () => import(/* webpackChunkName: "v-40a1e883" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/distributed-system/idempotent.html.js").then(({ data }) => data),
  // path: /distributed-system/raft.html
  "v-df954d82": () => import(/* webpackChunkName: "v-df954d82" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/distributed-system/raft.html.js").then(({ data }) => data),
  // path: /distributed-system/transaction.html
  "v-cc8a0c50": () => import(/* webpackChunkName: "v-cc8a0c50" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/distributed-system/transaction.html.js").then(({ data }) => data),
  // path: /http/compress.html
  "v-68be2829": () => import(/* webpackChunkName: "v-68be2829" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/http/compress.html.js").then(({ data }) => data),
  // path: /http/dns-ha.html
  "v-6f475e64": () => import(/* webpackChunkName: "v-6f475e64" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/http/dns-ha.html.js").then(({ data }) => data),
  // path: /http/dns.html
  "v-5c4d689e": () => import(/* webpackChunkName: "v-5c4d689e" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/http/dns.html.js").then(({ data }) => data),
  // path: /http/http-dns.html
  "v-3997b332": () => import(/* webpackChunkName: "v-3997b332" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/http/http-dns.html.js").then(({ data }) => data),
  // path: /http/https.html
  "v-39095ac8": () => import(/* webpackChunkName: "v-39095ac8" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/http/https.html.js").then(({ data }) => data),
  // path: /http/intro.html
  "v-7db9848a": () => import(/* webpackChunkName: "v-7db9848a" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/http/intro.html.js").then(({ data }) => data),
  // path: /http/latency.html
  "v-ab90322e": () => import(/* webpackChunkName: "v-ab90322e" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/http/latency.html.js").then(({ data }) => data),
  // path: /http/nginx-quic.html
  "v-0f1473da": () => import(/* webpackChunkName: "v-0f1473da" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/http/nginx-quic.html.js").then(({ data }) => data),
  // path: /http/protobuf.html
  "v-12d38cc0": () => import(/* webpackChunkName: "v-12d38cc0" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/http/protobuf.html.js").then(({ data }) => data),
  // path: /http/quic-performance.html
  "v-212fac6c": () => import(/* webpackChunkName: "v-212fac6c" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/http/quic-performance.html.js").then(({ data }) => data),
  // path: /http/quic.html
  "v-5b853866": () => import(/* webpackChunkName: "v-5b853866" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/http/quic.html.js").then(({ data }) => data),
  // path: /http/ssl-performance.html
  "v-454672d0": () => import(/* webpackChunkName: "v-454672d0" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/http/ssl-performance.html.js").then(({ data }) => data),
  // path: /http/ssl.html
  "v-6f0c459b": () => import(/* webpackChunkName: "v-6f0c459b" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/http/ssl.html.js").then(({ data }) => data),
  // path: /kubernetes/AutoScaling.html
  "v-430a239e": () => import(/* webpackChunkName: "v-430a239e" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/kubernetes/AutoScaling.html.js").then(({ data }) => data),
  // path: /kubernetes/CNI.html
  "v-1f11f12a": () => import(/* webpackChunkName: "v-1f11f12a" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/kubernetes/CNI.html.js").then(({ data }) => data),
  // path: /kubernetes/CRI.html
  "v-77e01722": () => import(/* webpackChunkName: "v-77e01722" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/kubernetes/CRI.html.js").then(({ data }) => data),
  // path: /kubernetes/component.html
  "v-7d74e30c": () => import(/* webpackChunkName: "v-7d74e30c" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/kubernetes/component.html.js").then(({ data }) => data),
  // path: /kubernetes/cpu-resource.html
  "v-838581d4": () => import(/* webpackChunkName: "v-838581d4" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/kubernetes/cpu-resource.html.js").then(({ data }) => data),
  // path: /kubernetes/deployment.html
  "v-16537224": () => import(/* webpackChunkName: "v-16537224" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/kubernetes/deployment.html.js").then(({ data }) => data),
  // path: /kubernetes/
  "v-21e54510": () => import(/* webpackChunkName: "v-21e54510" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/kubernetes/index.html.js").then(({ data }) => data),
  // path: /kubernetes/ingress.html
  "v-b890b758": () => import(/* webpackChunkName: "v-b890b758" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/kubernetes/ingress.html.js").then(({ data }) => data),
  // path: /kubernetes/job.html
  "v-7fc8af68": () => import(/* webpackChunkName: "v-7fc8af68" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/kubernetes/job.html.js").then(({ data }) => data),
  // path: /kubernetes/kube-proxy.html
  "v-7c477abb": () => import(/* webpackChunkName: "v-7c477abb" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/kubernetes/kube-proxy.html.js").then(({ data }) => data),
  // path: /kubernetes/namespace.html
  "v-88a99524": () => import(/* webpackChunkName: "v-88a99524" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/kubernetes/namespace.html.js").then(({ data }) => data),
  // path: /kubernetes/pod.html
  "v-32cc66c4": () => import(/* webpackChunkName: "v-32cc66c4" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/kubernetes/pod.html.js").then(({ data }) => data),
  // path: /kubernetes/pv.html
  "v-0274ef83": () => import(/* webpackChunkName: "v-0274ef83" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/kubernetes/pv.html.js").then(({ data }) => data),
  // path: /kubernetes/pvp.html
  "v-8cdba602": () => import(/* webpackChunkName: "v-8cdba602" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/kubernetes/pvp.html.js").then(({ data }) => data),
  // path: /kubernetes/requests-limits.html
  "v-40f22770": () => import(/* webpackChunkName: "v-40f22770" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/kubernetes/requests-limits.html.js").then(({ data }) => data),
  // path: /kubernetes/resource.html
  "v-11c96b0a": () => import(/* webpackChunkName: "v-11c96b0a" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/kubernetes/resource.html.js").then(({ data }) => data),
  // path: /kubernetes/service.html
  "v-83d6df58": () => import(/* webpackChunkName: "v-83d6df58" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/kubernetes/service.html.js").then(({ data }) => data),
  // path: /kubernetes/virtual.html
  "v-fe320484": () => import(/* webpackChunkName: "v-fe320484" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/kubernetes/virtual.html.js").then(({ data }) => data),
  // path: /network/bbr.html
  "v-f2c3e0be": () => import(/* webpackChunkName: "v-f2c3e0be" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/network/bbr.html.js").then(({ data }) => data),
  // path: /network/kernel-bypass.html
  "v-19b72a9b": () => import(/* webpackChunkName: "v-19b72a9b" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/network/kernel-bypass.html.js").then(({ data }) => data),
  // path: /network/kernel.html
  "v-15c5767c": () => import(/* webpackChunkName: "v-15c5767c" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/network/kernel.html.js").then(({ data }) => data),
  // path: /network/netfilter.html
  "v-4c016a44": () => import(/* webpackChunkName: "v-4c016a44" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/network/netfilter.html.js").then(({ data }) => data),
  // path: /network/tcp.html
  "v-05efc152": () => import(/* webpackChunkName: "v-05efc152" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/network/tcp.html.js").then(({ data }) => data),
  // path: /serverless/Knative.html
  "v-2379a29e": () => import(/* webpackChunkName: "v-2379a29e" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/serverless/Knative.html.js").then(({ data }) => data),
  // path: /serverless/intro.html
  "v-f1ab8db2": () => import(/* webpackChunkName: "v-f1ab8db2" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/serverless/intro.html.js").then(({ data }) => data),
  // path: /content/chapter1/SDN.html
  "v-bb9633d0": () => import(/* webpackChunkName: "v-bb9633d0" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/SDN.html.js").then(({ data }) => data),
  // path: /content/chapter1/anycast.html
  "v-b9e77c6c": () => import(/* webpackChunkName: "v-b9e77c6c" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/anycast.html.js").then(({ data }) => data),
  // path: /content/chapter1/bgp.html
  "v-2de5662a": () => import(/* webpackChunkName: "v-2de5662a" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/bgp.html.js").then(({ data }) => data),
  // path: /content/chapter1/bridge.html
  "v-033d7634": () => import(/* webpackChunkName: "v-033d7634" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/bridge.html.js").then(({ data }) => data),
  // path: /content/chapter1/congestion-control.html
  "v-992f1274": () => import(/* webpackChunkName: "v-992f1274" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/congestion-control.html.js").then(({ data }) => data),
  // path: /content/chapter1/dpdk.html
  "v-4850faea": () => import(/* webpackChunkName: "v-4850faea" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/dpdk.html.js").then(({ data }) => data),
  // path: /content/chapter1/edge.html
  "v-db062500": () => import(/* webpackChunkName: "v-db062500" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/edge.html.js").then(({ data }) => data),
  // path: /content/chapter1/
  "v-bb155188": () => import(/* webpackChunkName: "v-bb155188" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/index.html.js").then(({ data }) => data),
  // path: /content/chapter1/internet.html
  "v-5e94247c": () => import(/* webpackChunkName: "v-5e94247c" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/internet.html.js").then(({ data }) => data),
  // path: /content/chapter1/intro.html
  "v-8a710bae": () => import(/* webpackChunkName: "v-8a710bae" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/intro.html.js").then(({ data }) => data),
  // path: /content/chapter1/kernel-by-pass.html
  "v-6f1d5846": () => import(/* webpackChunkName: "v-6f1d5846" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/kernel-by-pass.html.js").then(({ data }) => data),
  // path: /content/chapter1/latency.html
  "v-68828052": () => import(/* webpackChunkName: "v-68828052" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/latency.html.js").then(({ data }) => data),
  // path: /content/chapter1/mtr.html
  "v-57ad074a": () => import(/* webpackChunkName: "v-57ad074a" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/mtr.html.js").then(({ data }) => data),
  // path: /content/chapter1/net-optimize-intro.html
  "v-a0fb701e": () => import(/* webpackChunkName: "v-a0fb701e" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/net-optimize-intro.html.js").then(({ data }) => data),
  // path: /content/chapter1/net-optimize.html
  "v-7a8d87a0": () => import(/* webpackChunkName: "v-7a8d87a0" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/net-optimize.html.js").then(({ data }) => data),
  // path: /content/chapter1/network-namespace.html
  "v-ef45ffce": () => import(/* webpackChunkName: "v-ef45ffce" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/network-namespace.html.js").then(({ data }) => data),
  // path: /content/chapter1/route.html
  "v-b383df28": () => import(/* webpackChunkName: "v-b383df28" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/route.html.js").then(({ data }) => data),
  // path: /content/chapter1/tcp-handshake.html
  "v-635f36ea": () => import(/* webpackChunkName: "v-635f36ea" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/tcp-handshake.html.js").then(({ data }) => data),
  // path: /content/chapter1/tcp-handwave.html
  "v-0f386d6e": () => import(/* webpackChunkName: "v-0f386d6e" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/tcp-handwave.html.js").then(({ data }) => data),
  // path: /content/chapter1/tcpdump.html
  "v-b313ec40": () => import(/* webpackChunkName: "v-b313ec40" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/tcpdump.html.js").then(({ data }) => data),
  // path: /content/chapter1/transport.html
  "v-7b254ce8": () => import(/* webpackChunkName: "v-7b254ce8" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/transport.html.js").then(({ data }) => data),
  // path: /content/chapter1/tun.html
  "v-5ce81a28": () => import(/* webpackChunkName: "v-5ce81a28" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/tun.html.js").then(({ data }) => data),
  // path: /content/chapter1/underlay.html
  "v-b8cb8a5e": () => import(/* webpackChunkName: "v-b8cb8a5e" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/underlay.html.js").then(({ data }) => data),
  // path: /content/chapter1/veth-pair.html
  "v-246d8491": () => import(/* webpackChunkName: "v-246d8491" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/veth-pair.html.js").then(({ data }) => data),
  // path: /content/chapter1/vxlan.html
  "v-29265fde": () => import(/* webpackChunkName: "v-29265fde" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/chapter1/vxlan.html.js").then(({ data }) => data),
  // path: /content/product/finops.html
  "v-b1c9dfba": () => import(/* webpackChunkName: "v-b1c9dfba" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/content/product/finops.html.js").then(({ data }) => data),
  // path: /404.html
  "v-3706649a": () => import(/* webpackChunkName: "v-3706649a" */"/Users/mac/Desktop/codes/html/theByteBook/.vuepress/.temp/pages/404.html.js").then(({ data }) => data),
}
