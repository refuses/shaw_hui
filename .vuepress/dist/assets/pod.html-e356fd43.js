import{_ as e,p as d,q as t,a1 as n}from"./framework-5866ffd3.js";const i={},a=n(`<h1 id="pod" tabindex="-1"><a class="header-anchor" href="#pod" aria-hidden="true">#</a> Pod</h1><p>笔者在前面介绍说，容器本质是个资源隔离的特殊进程，而对于 Pod ，则可以进一步理解为资源隔离一组特殊进程。</p><p>容器的设计哲学是希望在一个容器里面之运行一个进程，那么针对这个进程的辅助程序该如何设计呢，比如监控程序，再比如实现进程强耦合的Sidecar程序。如果按照容器的设计哲学进行思考，这就变得复杂起来。</p><p>Kubernetes 创新地将多个容器组合到一起，产生一个新的，也是Kubernetes最基本的管理资源单位 Pod。</p><p>每个 Pod 由一组容器构建，这些容器在集群同一个节点运行，共享相同的内部网络和存储等资源，他们之间相互协作，共同完成某一类特定的服务。这个设计思想最经典的体现在于 Service Mesh的实现，比如一个常规的服务，通过部署一个 Sidecar 代理容器，从来无缝地实现熔断限流、服务发现、负载均衡等功能。这种方式在监控、数据指标采集 Prometheus 等也大量采用，通过对每类对象开发相应的 export 完成数据指标的采集。</p><p>总而言之，就是通过 Pod 的设计方式，将一组紧密相关的进程共享网络、存储等资源，通过对 Pod 生命周期的管理，从而完成这一组容器的生命周期管理。</p><h2 id="创建一个-pod" tabindex="-1"><a class="header-anchor" href="#创建一个-pod" aria-hidden="true">#</a> 创建一个 Pod</h2><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>apiVersion: v1                      # Kubernetes的API Version
kind: Pod                           # Kubernetes的资源类型
metadata:
  name: nginx                       # Pod的名称
spec:                               # Pod的具体规格（specification）
  containers:
  - image: nginx:alpine             # 使用的镜像为 nginx:alpine
    name: container-0               # 容器的名称
    resources:                      # 申请容器所需的资源
      limits:
        cpu: 100m
        memory: 200Mi
      requests:
        cpu: 100m
        memory: 200Mi
  imagePullSecrets:                 # 拉取镜像使用的证书，在CCE上必须为default-secret
  - name: default-secret
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>Pod创建完成后，可以使用kubectl get pods命令查询Pod的状态，如下所示。</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>$ kubectl get pods

NAME           READY   STATUS    RESTARTS   AGE
nginx          1/1     Running   0          40s
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="pod-生命周期" tabindex="-1"><a class="header-anchor" href="#pod-生命周期" aria-hidden="true">#</a> Pod 生命周期</h2><table><thead><tr><th style="text-align:left;">阶段</th><th style="text-align:left;">说明</th></tr></thead><tbody><tr><td style="text-align:left;">Pending</td><td style="text-align:left;">Pod 已经被 Kubernetes 接受（存在 etcd 中），等待 scheduler 调度 或者 调度成功拉取镜像</td></tr><tr><td style="text-align:left;">Running</td><td style="text-align:left;">Pod 中所有的容器都已创建，并且至少有一个容器正在运行</td></tr><tr><td style="text-align:left;">Succeeded</td><td style="text-align:left;">Pod 中所有的容器都被成功终止，并且不会重启，这个主要是执行 job 任务</td></tr><tr><td style="text-align:left;">Failed</td><td style="text-align:left;">Pod 中所有的容器都已经终止，并且至少有一个容器是非正常终止</td></tr><tr><td style="text-align:left;">Unknown</td><td style="text-align:left;">无法获取 pod状态，通常是由于 Pod 节点通信出错，或者 kubelet 宕机所致</td></tr></tbody></table><h2 id="pod-的网络" tabindex="-1"><a class="header-anchor" href="#pod-的网络" aria-hidden="true">#</a> Pod 的网络</h2><p>以 Calico 为例，Kubernetes会默认为每个pod分配一个IP地址，所有的Pod IP地址在同一个网段， kubeadm 在初始化时指定网段参数</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>kubeadm init --pod-network-cidr=192.168.2.0/24
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><p>Pod 中所有的容器会共享该 Pod 的IP地址，因此 Pod 内容器间通信，直接用 locahost+port 即可。</p>`,16),l=[a];function s(r,o){return d(),t("div",null,l)}const u=e(i,[["render",s],["__file","pod.html.vue"]]);export{u as default};
