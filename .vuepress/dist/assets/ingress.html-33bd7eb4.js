import{_ as e,p as n,q as s,a1 as i}from"./framework-5866ffd3.js";const r="/assets/ingress-c8ca534f.png",d="/assets/ingress-controller-3ad9bd2a.png",a={},t=i('<h1 id="ingress" tabindex="-1"><a class="header-anchor" href="#ingress" aria-hidden="true">#</a> Ingress</h1><p>在 Kubernetes 中，为了使外部的应用能够访问集群内的 service，最为常用的是使用 NodePort 和 LoadBalancer 两种类型的 service，但它们在使用上还是有一些限制，譬如对外提供访问时，NodePort 类型需要在外部搭建额外的负载均衡，其次 NodePort 会占用很多集群机器的端口。而 LoadBalancer 要求 Kubernetes 必须跑在支持的 Cloud Provider 上，由云厂商提供公网 IP 地址，当同时当存在多个 LoadBalancer 的类型 Service 时，会占用大量公网 ip 地址。 以上的问题有没有解决方案呢？ 答案是使用 Ingress。</p><p>Ingress 是英文入口的意思，在 Kubernetes 中，Ingress 用于定义如何从集群外部访问 Kubernetes 内部服务，是对集群中服务的外部访问进行管理的API对象。典型的访问方式，譬如 HTTP 和 HTTPS，另外也可提供负载均衡、SSL 卸载和基于名称的虚拟托管服务等功能。</p><div align="center"><img src="'+r+'" width="600" align="center"></div><h2 id="ingress-的工作机制" tabindex="-1"><a class="header-anchor" href="#ingress-的工作机制" aria-hidden="true">#</a> Ingress 的工作机制</h2><p>要想使用 Ingress 功能，必须在 Kubernetes 集群上安装Ingress Controller。Ingress Controller 有很多种实现，最常见的就是 Kubernetes 官方维护的 NGINX Ingress Controller。</p><p>外部请求首先到达 Ingress Controller，Ingress Controller 根据 Ingress 的路由规则，查找到对应的Service，进而通过 Endpoint 查询到 Pod 的 IP 地址，然后将请求转发给 Pod。</p><div align="center"><img src="'+d+`" width="600" align="center"></div><h2 id="创建ingress" tabindex="-1"><a class="header-anchor" href="#创建ingress" aria-hidden="true">#</a> 创建Ingress</h2><p>下面例子中，使用 http 协议，关联的后端 Service 为 <code>nginx:8080</code>，使用 ELB 作为 Ingress 控制器（metadata.annotations字段都是指定使用哪个ELB实例），当访问<code>http://192.168.10.155:8080/test”时，流量转发“nginx:8080</code>对应的 Service，从而将流量转发到对应Pod。</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: test-ingress
  annotations:
    kubernetes.io/ingress.class: cce
    kubernetes.io/elb.port: &#39;8080&#39;
    kubernetes.io/elb.ip: 192.168.10.155
    kubernetes.io/elb.id: aa7cf5ec-7218-4c43-98d4-c36c0744667a
spec:
  rules:
  - host: &#39;&#39;
    http:
      paths:
      - backend:
          serviceName: nginx
          servicePort: 8080
        path: &quot;/test&quot;
        property:
          ingress.beta.kubernetes.io/url-match-mode: STARTS_WITH
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>Ingress中还可以设置外部域名，这样您就可以通过域名来访问到ELB，进而访问到后端服务。</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>spec:
  rules:
  - host: www.thebyte.com.cn      # 域名
    http:
      paths:
      - path: /
        backend:
          serviceName: nginx
          servicePort: 80

</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="路由到多个服务" tabindex="-1"><a class="header-anchor" href="#路由到多个服务" aria-hidden="true">#</a> 路由到多个服务</h2><p>Ingress可以同时路由到多个服务，配置如下所示。</p><ul><li>当访问“http://www.thebyte.com.cn/test”时，访问的是“s1:80”后端。</li><li>当访问“http://www.thebyte.com.cn/demo”时，访问的是“s2:80”后端。</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>spec:
  rules:
  - host: www.thebyte.com.cn  # host地址
    http:
      paths:
      - path: &quot;/test&quot;
        backend:
          serviceName: s1
          servicePort: 80
      - path: &quot;/demo&quot;
        backend:
          serviceName: s2
          servicePort: 80
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,17),l=[t];function c(v,o){return n(),s("div",null,l)}const b=e(a,[["render",c],["__file","ingress.html.vue"]]);export{b as default};
