import{_ as e,p as n,q as i,a1 as s}from"./framework-5866ffd3.js";const a="/assets/kubelet-d773320d.png",l={},d=s(`<h1 id="使用多运行时-runtimeclass" tabindex="-1"><a class="header-anchor" href="#使用多运行时-runtimeclass" aria-hidden="true">#</a> 使用多运行时 RuntimeClass</h1><p>RuntimeClass 是 v1.12 引入的新 API 对象，用来支持多容器运行时，比如</p><ul><li>Kata Containers/gVisor + runc</li><li>Windows Process isolation + Hyper-V isolation containers</li></ul><p>RuntimeClass 表示一个运行时对象，在使用前需要开启特性开关 RuntimeClass，并创建 RuntimeClass CRD：</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>kubectl apply -f https://github.com/kubernetes/kubernetes/tree/master/cluster/addons/runtimeclass/runtimeclass_crd.yaml
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><p>然后就可以定义 RuntimeClass 对象</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>apiVersion: node.k8s.io/v1alpha1  # RuntimeClass is defined in the node.k8s.io API group
kind: RuntimeClass
metadata:
  name: myclass  # The name the RuntimeClass will be referenced by
  # RuntimeClass is a non-namespaced resource
spec:
  runtimeHandler: myconfiguration  # The name of the corresponding CRI configuration

</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>在 Pod 中定义使用哪个 RuntimeClass：</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  runtimeClassName: myclass

</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="小结" tabindex="-1"><a class="header-anchor" href="#小结" aria-hidden="true">#</a> 小结</h2><p>梳理完 CRI 的发展关系后，总结 kubelet 调用 各容器运行时 关系如下图所示：</p><div align="center"><img src="`+a+'" width="600" align="center"></div>',12),t=[d];function r(c,u){return n(),i("div",null,t)}const o=e(l,[["render",r],["__file","RuntimeClass.html.vue"]]);export{o as default};
